#include "LArCondUtils/LArFecLvTempDcsTool.h"
#include "LArCondUtils/LArHVToolMC.h" 
#include "LArCondUtils/LArHVToolDB.h" 
#include "LArCondUtils/LArFEBTempTool.h"
#include "LArCondUtils/LArHVPathologyDbAlg.h"
#include "LArCondUtils/LArHV2Ntuple.h"



DECLARE_COMPONENT( LArFecLvTempDcsTool )
DECLARE_COMPONENT( LArHVToolMC )
DECLARE_COMPONENT( LArHVToolDB )
DECLARE_COMPONENT( LArFEBTempTool )
DECLARE_COMPONENT( LArHVPathologyDbAlg )
DECLARE_COMPONENT( LArHV2Ntuple )

