/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/


/**

@page TrigEgammaRec_page 
@author P. Urquijo

@section TrigEgammaRec_MyPackageOverview Overview
This package contains the Event Filter reconstruction trigger algorithms for the Electron and Photon slices.   Algorithms are adaptations of the offline/Reconstruction/egammaRec/egammaBuilder.cxx



*/
